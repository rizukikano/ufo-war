﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text scoredisplay;

    // Start is called before the first frame update
    void Start()
    {
        scoredisplay.text = PlayerPrefs.GetInt("Score").ToString();
    }
    public void PreviousScene()
    {
        SceneManager.LoadScene(PlayerPrefs.GetString("PrevScene"));
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
