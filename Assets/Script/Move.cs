﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    //Leaderboard leaderboard;
    public static int[] lead;
    private readonly int arrayscore = 10;
    public Text scoreDisplay;
    public Text HPDisplay;
    public Text ExpDisplay;
    public Text LevelDisplay;
    public float health = 10;
    public float armor;
    public int playerLevel;
    public float damage;
    public float playerdamage;
    public float playerspeed;
    public float playerarmor;
    public int playerexp;
    public static int score;
    public static int scoreend;
    Rigidbody2D body;
    public ParticleSystem green;
    public ParticleSystem red;
    public ParticleSystem blue;
    private SFX sfx;

    float horizontal;
    float vertical;
    public float moveLimiter;

    public float runSpeed = 20.0f;

    void Start()
    {
        playerexp = 0;
        health = 10;
        body = GetComponent<Rigidbody2D>();
        playerLevel = 1;
        score = 0;
        lead = new int[arrayscore];
        sfx = GameObject.FindGameObjectWithTag("SFX").GetComponent<SFX>();




    }

    void Update()
    {
        health = Mathf.Round(health * 100f) / 100f;
        HPDisplay.text = "HP : " + health;
        scoreDisplay.text = score.ToString();
        ExpDisplay.text = playerexp + "/100";
        LevelDisplay.text = "Level " + playerLevel;

        if (health <= 0)
        {
            scoreend = score;
           
            PlayerPrefs.SetInt("Score", scoreend);
            PlayerPrefs.SetString("PrevScene", SceneManager.GetActiveScene().name);
           // Debug.Log(PlayerPrefs.GetInt("Score"));
            if (PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore5")&&
               PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore4")&&
               PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore3")&&
               PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore2") &&
               PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore1"))
            {
                PlayerPrefs.SetInt("HighScore5", PlayerPrefs.GetInt("HighScore4"));
                PlayerPrefs.SetInt("HighScore4", PlayerPrefs.GetInt("HighScore3"));
                PlayerPrefs.SetInt("HighScore3", PlayerPrefs.GetInt("HighScore2"));
                PlayerPrefs.SetInt("HighScore2", PlayerPrefs.GetInt("HighScore1"));
                PlayerPrefs.SetInt("HighScore1", PlayerPrefs.GetInt("Score"));
            } else if (PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore5") &&
               PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore4") &&
               PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore3") &&
               PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore2") &&
               PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore1"))
            {
                PlayerPrefs.SetInt("HighScore5", PlayerPrefs.GetInt("HighScore4"));
                PlayerPrefs.SetInt("HighScore4", PlayerPrefs.GetInt("HighScore3"));
                PlayerPrefs.SetInt("HighScore3", PlayerPrefs.GetInt("HighScore2"));
                PlayerPrefs.SetInt("HighScore2", PlayerPrefs.GetInt("Score"));
            }
            else if (PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore5") &&
              PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore4") &&
              PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore3") &&
              PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore2") &&
              PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore1"))
            {
                PlayerPrefs.SetInt("HighScore5", PlayerPrefs.GetInt("HighScore4"));
                PlayerPrefs.SetInt("HighScore4", PlayerPrefs.GetInt("HighScore3"));
                PlayerPrefs.SetInt("HighScore3", PlayerPrefs.GetInt("Score"));
            }
            else if (PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore5") &&
             PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore4") &&
             PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore3") &&
             PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore2") &&
             PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore1"))
            {
                PlayerPrefs.SetInt("HighScore5", PlayerPrefs.GetInt("HighScore4"));
                PlayerPrefs.SetInt("HighScore4", PlayerPrefs.GetInt("Score"));
            }
            else if (PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore5") &&
            PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore4") &&
            PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore3") &&
            PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore2") &&
            PlayerPrefs.GetInt("Score") < PlayerPrefs.GetInt("HighScore1"))
            {
                PlayerPrefs.SetInt("HighScore5", PlayerPrefs.GetInt("Score"));
            }
            sfx.gameover.Play();
            SceneManager.LoadScene("GameOver");
        }
        // Gives a value between -1 and 1
        horizontal = Input.GetAxisRaw("Horizontal"); // -1 is left
        vertical = Input.GetAxisRaw("Vertical"); // -1 is down
        damage = playerLevel * playerdamage;
        moveLimiter = playerspeed * playerLevel;
        armor = playerLevel * playerarmor;
        if (playerexp >= 100)
        {
            playerLevel +=1;
            playerexp = 0;
        }
    }   
    void FixedUpdate()
    {
        if (horizontal != 0 && vertical != 0) // Check for diagonal movement
        {
            // limit movement speed diagonally, so you move at 70% speed
            horizontal *= moveLimiter;
            vertical *= moveLimiter;
        }

        body.velocity = new Vector2(horizontal * runSpeed, vertical * runSpeed);
    }
}
