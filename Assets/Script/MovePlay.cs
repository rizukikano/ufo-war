﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MovePlay : MonoBehaviour
{
    public GameObject current;
    public GameObject next;
    public Button button;
    public string mode;
    private SFXHandler sfx;
    private BGMHandler bgm;
    // Use this for initialization
    void Start()
    {
        sfx = GameObject.FindWithTag("Handler").GetComponent<SFXHandler>();
        bgm = GameObject.FindWithTag("Handler").GetComponent<BGMHandler>();
        button.onClick.AddListener(ButtonClick);

    }

    public void ButtonClick()
    {
        current.SetActive(false);
        next.SetActive(true);
        PlayerPrefs.SetString("checksfx", sfx.check.ToString());
        PlayerPrefs.SetString("checkbgm", bgm.check.ToString());
        SceneManager.LoadScene(mode);
        Debug.Log(PlayerPrefs.GetString("checksfx"));
        Debug.Log(PlayerPrefs.GetString("checkbgm"));

    }
    // Update is called once per frame
    void Update()
    {


    }
}
