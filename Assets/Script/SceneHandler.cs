﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public string sceneName;
    public GameObject[] logos;

    void Start()
    {
        StartCoroutine(changeScene());
    }

    IEnumerator changeScene()
    {
        foreach (var item in logos)
        {
            FadeHandler fade = item.GetComponent<FadeHandler>();
            fade.startFade();

            yield return new WaitForSeconds(3f);
        }

        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
