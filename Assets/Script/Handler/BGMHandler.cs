﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMHandler : MonoBehaviour
{
    private BGM bgm;
    public bool check = true;
    public GameObject centang;
    public GameObject nocentang;
    private string checktmp;
    // Start is called before the first frame update
    void Start()
    {
        bgm = GameObject.FindWithTag("BGM").GetComponent<BGM>();
        //check = true;
        checktmp = PlayerPrefs.GetString("checkbgm");
        if (checktmp == "True")
        {
            check = true;
            centang.SetActive(true);
            nocentang.SetActive(false);
        }
        else if (checktmp == "False")
        {
            check = false;
            centang.SetActive(false);
            nocentang.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ButtonBGM()
    {
        if (check == true)
        {
            bgm.bgm.mute = !bgm.bgm.mute;
            check = false;
            centang.SetActive(false);
            nocentang.SetActive(true);
        }
        else if (check == false)
        {
            bgm.bgm.mute = !bgm.bgm.mute;
            check = true;
            centang.SetActive(true);
            nocentang.SetActive(false);
        }
    }
}
