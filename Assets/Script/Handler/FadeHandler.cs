﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeHandler : MonoBehaviour
{
    private SpriteRenderer render;
    
    private void Awake() {
        render = gameObject.GetComponent<SpriteRenderer>();
        render.color = new Color(1, 1, 1, 0);
    }

    public void startFade()
    {
        StartCoroutine(FadeImage(true));
    }

    IEnumerator FadeImage(bool fadeIn)
    {
        if (fadeIn) {
            StartCoroutine(doFadeIn());
            yield return new WaitForSeconds(2f);
            StartCoroutine(doFadeOut());
        }
        else {
            StartCoroutine(doFadeOut());
            yield return new WaitForSeconds(2f);
            StartCoroutine(doFadeIn());
        }
    }

    IEnumerator doFadeIn()
    {
        for (float i = 0; i <= 1; i += Time.deltaTime)
        {
            // set color with i as alpha
            render.color = new Color(1, 1, 1, i);
            yield return null;
        }
    }

    IEnumerator doFadeOut()
    {
        for (float i = 1; i >= 0; i -= Time.deltaTime)
        {
            // set color with i as alpha
            render.color = new Color(1, 1, 1, i);
            yield return null;
        }
    }
}
