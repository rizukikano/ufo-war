﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXHandler : MonoBehaviour
{
    private SFX sfx;
    public bool check = true ;
    private string checktmp;
    public GameObject centang;
    public GameObject nocentang;
    // Start is called before the first frame update
    void Start()
    {
        sfx = GameObject.FindWithTag("SFX").GetComponent<SFX>();
        //check = true;

        checktmp = PlayerPrefs.GetString("checksfx");
        Debug.Log(checktmp);
        if (checktmp == "True")
        {
            check = true;
            centang.SetActive(true);
            nocentang.SetActive(false);
        } else if (checktmp == "False")
       {
            check = false;
            centang.SetActive(false);
            nocentang.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

      
        
    }
    public void ButtonSfx()
    {
        if(check == true)
        {

            sfx.hit.mute = !sfx.hit.mute;
            sfx.destroy.mute = !sfx.destroy.mute;
            sfx.gameover.mute = !sfx.gameover.mute;
            check = false;
            centang.SetActive(false);
            nocentang.SetActive(true);
        } else if (check == false)
        {
            sfx.hit.mute = !sfx.hit.mute;
            sfx.destroy.mute = !sfx.destroy.mute;
            sfx.gameover.mute = !sfx.gameover.mute;
            check = true;
            centang.SetActive(true);
            nocentang.SetActive(false);
        }
    }
}
