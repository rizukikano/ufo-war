﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRed : MonoBehaviour
{
    public float speed;
    private Transform playerPos;
    private Move player;
    protected float enemyHP;
    protected float enemyDamage;
    private SFX sfx;


    // Start is called before the first frame update
    void Start() { 
        playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Move>();
        sfx = GameObject.FindGameObjectWithTag("SFX").GetComponent<SFX>();
        enemyHP = Random.Range(1, 4) + (2 * player.playerLevel);
        enemyDamage = Random.Range(1, 3) + player.damage;
        speed = (5 + (1.2f * player.playerLevel));
        

        //Debug.Log(player);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, playerPos.position, speed * Time.deltaTime);
        if (enemyHP <=0)
        {
            Move.score += 1;
            player.playerexp += 10;
            //Debug.Log(player.playerexp);
            sfx.destroy.Play();
            Destroy(gameObject);
            ParticleSystem probaParticleClone = Instantiate(player.red, transform.position, Quaternion.identity) as ParticleSystem;
            probaParticleClone.Play();



        } ;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Move.score += 1;
            player.playerexp += 10;
            player.health -= (enemyDamage - player.armor);
            //Debug.Log(player.playerexp);
            sfx.destroy.Play();
            Destroy(gameObject);
            ParticleSystem probaParticleClone = Instantiate(player.red, transform.position, Quaternion.identity) as ParticleSystem;
            probaParticleClone.Play();

        }
        if (other.CompareTag("ProjectileBlue")){
            enemyHP -= (2* player.damage);
            //Debug.Log(enemyHP);
            sfx.hit.Play();
            Destroy(other.gameObject);
           // Destroy(gameObject);
        }
        if (other.CompareTag("ProjectileRed"))
        {
            enemyHP -= (1 * player.damage);
            //Debug.Log(enemyHP);
            sfx.hit.Play();
            Destroy(other.gameObject);
            // Destroy(gameObject);
        }
        if (other.CompareTag("ProjectileGreen"))
        {
            enemyHP -= (0.5f * player.damage);
            //Debug.Log(enemyHP);
            sfx.hit.Play();
            Destroy(other.gameObject);
            // Destroy(gameObject);
        }
    }
}
