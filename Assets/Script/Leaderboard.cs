﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{
    public static int tempscore;

    public Text[] scoreDisplay;
    // Start is called before the first frame update
    void Start()
    {

        for (int i = 1; i < 6; i++)
        {
            scoreDisplay[i-1].text = i + ". " +PlayerPrefs.GetInt("HighScore"+i);
        }
        Debug.Log(PlayerPrefs.GetInt("HighScore1"));


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Awake()
    {
        
    }
    public void MoveMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
