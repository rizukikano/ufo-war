﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject[] projectile;
    private Transform playerPos;
    private int a;
    // Start is called before the first frame update
    void Start()
    {
        playerPos = GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetMouseButtonDown(0))
        {

            Instantiate(projectile[a], playerPos.position, Quaternion.identity);
            
            
        }
        
    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            a = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            a = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            a = 2;
        }
    }
}
